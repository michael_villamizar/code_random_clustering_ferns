/* 

	mex_rcfs_test

	Michael Villamizar
	mvillami@iri.upc.edu
	Institut de Robòtica i Informàtica Industrial CSIC-UPC
	Barcelona
	2016

	Description:
 		This function tests the Random Clustering Ferns (RCFs) for multimodal 
		object recognition on the input image. RCFs use a Boosted Random Ferns 
		(BRFs) classifier and probabilistic Latent Semantic Analysis (pLSA) to 
		detect the object and discover the intra-class appearance clusters. To 
		speed up the detection process, this function works on the fern output 
		maps which are computed in advance.

 	Input: 
		prhs[0] -> fern output maps 
		prhs[1] -> weak classifiers (WCs) data -fern location and index-
		prhs[2] -> weak classifiers (WCs) probabilities -histograms-
		prhs[3] -> object size
		prhs[4] -> num. max. detection outputs

	Output:
		plhs[0] -> detection data
		plhs[1] -> detection map

*/

#include <math.h>
#include "mex.h"
#include <stdio.h>

// main function
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) { 

	// check
	if(nrhs!=8) { mexErrMsgTxt("Five inputs required"); }
   if(nlhs!=2) { mexErrMsgTxt("Two output required"); }
 
	// parameters
	int minWCs = 50;  // min. number of weak classifier for naive cascade
	double thr = 0.4;  // classification threshold for naive cascade
	
	// copy input pointers
	mxArray *data0 = (mxArray *)prhs[0];
	mxArray *data1 = (mxArray *)prhs[1];
	mxArray *data2 = (mxArray *)prhs[2];
	mxArray *data3 = (mxArray *)prhs[3];
	mxArray *data4 = (mxArray *)prhs[4];
	mxArray *data5 = (mxArray *)prhs[5];
	mxArray *data6 = (mxArray *)prhs[6];
   mxArray *data7 = (mxArray *)prhs[7];

	// data pointers
	double *fernMaps = mxGetPr(data0);  // fern maps
	double *WCs = mxGetPr(data1);  // BRFs: weak classifiers data
	double *hstms = mxGetPr(data2);  // BRFs: weak classifiers probabilities
	double *objSize = mxGetPr(data3);  // object size
	int numHypotheses = (int)mxGetScalar(data4);  // num. output hypotheses
	int numClusters = (int)mxGetScalar(data5);  // num. intra-class clusters
	double *Pw_t = mxGetPr(data6);  // pLSA: prob. words and clusters (latent)
   double *Pt = mxGetPr(data7);  // pLSA:  prob. clusters (latent variables)

	// visual words
   const int *wordSize = mxGetDimensions(data6);  // visual words vector size
	int numWords = wordSize[0];  // num. visual words

	// map size	
	const int *mapSize = mxGetDimensions(data0);
	int sy = mapSize[0];  // image size in y
	int sx = mapSize[1];  // image size in x

	// number of weak classifiers
	int numWCs = mxGetM(data1);

	// number of histogram bins
	int numBins = mxGetN(data2);

	// output detection data
	int out[2];
	out[0] = numHypotheses;
	out[1] = 4;
	plhs[0] = mxCreateNumericArray(2, out, mxDOUBLE_CLASS, mxREAL);
	double *detData = (double *)mxGetPr(plhs[0]);

	// output detection map
	int out2[3];
	out2[0] = sy;
	out2[1] = sx;
	out2[2] = 2;
   plhs[1] = mxCreateNumericArray(3, out2, mxDOUBLE_CLASS, mxREAL);
	double *detMap = (double *)mxGetPr(plhs[1]);

	// image limits
	int ly = sy - (int)objSize[0];
	int lx = sx - (int)objSize[1];
	
	// variables
   double hist[numClusters];
	double score,value,maxVal;
	int fy,fx,fi,z,index,word,cluster;

	// scanning
	for (int y=0; y<ly; y++){
		for (int x=0; x<lx; x++){

			// detection score
			score = 0;

			// clear cluster distribution
		   for (int c=0; c<numClusters; c++){
           hist[c] = 0;
	      }   

			// test each weak classifier
			for (int wc=0; wc<numWCs; wc++){

				// weak classifier data: fern location and index
				fy = y + (int)*(WCs + wc + 0*numWCs) - 1;
				fx = x + (int)*(WCs + wc + 1*numWCs) - 1;
				fi =     (int)*(WCs + wc + 2*numWCs) - 1;

				// check
				if (fy<0 || fy>=sy || fx<0 || fx>=sx){ break; }

				// fern output associated with the current weak classifier
				z = (int)*(fernMaps + fy + fx*sy + fi*sy*sx) - 1;

				// check fern output
				if (z<0){ mexErrMsgTxt("Incorrect fern output"); }
			
				// update detection score
				score +=  *(hstms + wc + z*numWCs);
				
				// visual word
            word = numBins*wc + z;

				// update clusters distribution
            for (int c=0; c<numClusters; c++){
              hist[c] = hist[c] + *(Pw_t + word + c*numWords);  
	         }    
                                
            // naive cascade
				if ((wc>minWCs) & (score<=thr*wc)){ break; }
			
			}

			// max. cluster value
         cluster = -2;
			maxVal = 0;
         for (int c=0; c<numClusters; c++){
           if (hist[c]>maxVal){
             maxVal = hist[c];
             cluster = c;  
   	     }
         }   

			// detection maps
			*(detMap + y + x*sy + 0*sy*sx) = score/numWCs;  // scores
         *(detMap + y + x*sy + 1*sy*sx) = cluster;  // clusters

		}
	}

	// max. detection hypotheses
	for (int y=0; y<sy; y++){
		for (int x=0; x<sx; x++){

			// detection score and cluster 
			score = *(detMap + y + x*sy + 0*sy*sx);
         cluster = (int)*(detMap + y + x*sy + 1*sy*sx);

			// check 
			if (score<=0){ continue; }
		
			// comparing with previous values			
			index = -1;							
			for (int iter=numHypotheses-1; iter>=0; iter--){
				value = *(detData + iter + 2*numHypotheses);
				if (score>value){
					index = iter;
				}
			}
						
			// check
			if (index==-1){ continue; }

			// move previous results
			for (int iter=numHypotheses-1; iter>index; iter--){
				*(detData + iter + 0*numHypotheses) = *(detData + (iter-1) + 0*numHypotheses);
				*(detData + iter + 1*numHypotheses) = *(detData + (iter-1) + 1*numHypotheses);
				*(detData + iter + 2*numHypotheses) = *(detData + (iter-1) + 2*numHypotheses);
				*(detData + iter + 3*numHypotheses) = *(detData + (iter-1) + 3*numHypotheses);
			}
			
			// save detection data: location and score
			*(detData + index + 0*numHypotheses) = y + 1;
			*(detData + index + 1*numHypotheses) = x + 1;
			*(detData + index + 2*numHypotheses) = score;
         *(detData + index + 3*numHypotheses) = cluster+1;

		}
	}
}
