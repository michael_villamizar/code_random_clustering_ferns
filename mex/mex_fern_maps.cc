/* 

	mex_fern_maps

	Michael Villamizar
 	mvillami@iri.upc.edu
 	Institut de Robòtica i Informàtica Industrial CSIC-UPC
	Barcelona
	2016

 	Description:	
		This function convolves the Random Ferns -RFs- over the input images.
		
		At each image position, each fern is tested and the output computed.
		Testing a fern means testing every binary feature associated with this 
		fern. Binary features are comparisions between two image feature 
		values chosen at random. 
	
 	Input:
		prhs[0] <- input images (e.g HOG)
		prhs[1] <- fern data -feature locations and fern indexes- 
		prhs[2] <- fern size -pixels x pixels-

 Output:
		plhs[0] -> fern outputs

*/

#include <math.h>
#include "mex.h"
#include <stdio.h>

// Image size values
// This function sets the spatial image size [sy,sx], the number of image 
// feature channels -numChannels- and the number of input images.
inline void fun_image_size_values(const int* imgSize, int numDims, int &sy, int &sx, int &numChannels, int &numImgs){

  // image size properties
  if (numDims>1){
		
    // spatial image size
	 sy = imgSize[0];  // image size in y
	 sx = imgSize[1];  // image size in x

	 // check 3-dimension	
	 if (numDims>2){

	   // num. feature channels
	   numChannels = imgSize[2];
			
	   // check 4-dimension
	   if (numDims>3){
		  // num. images
		  numImgs = imgSize[3];
	   }
	 }
  }  
  else{
    mexErrMsgTxt("Incorrect input image format. The spatial dimension < 2");
  }
};

// main function
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) { 

	// check
	if(nrhs!=3) 
  	mexErrMsgTxt("Three inputs required: 1. images 2. fern data 3. fern size");
   if(nlhs!=1) 
  	mexErrMsgTxt("One output required: fern outputs");
  
	// copy input pointers
	mxArray *data0 = (mxArray *)prhs[0];
	mxArray *data1 = (mxArray *)prhs[1];
	mxArray *data2 = (mxArray *)prhs[2];

	// data pointers
	double *imgs = mxGetPr(data0);  // images
	double *data = mxGetPr(data1);  // fern data
	int fernSize = (int)mxGetScalar(data2);  // fern size

	// variables
	int sy = 1;  // image size in y
	int sx = 1;	 // image size in x
	int numImgs = 1;  // num. input images	
	int numChannels = 1;  // num. feature channels (e.g gradient orientations)

	// num. image dimensions
	int numDims = mxGetNumberOfDimensions(data0);

	// image size	
	const int *imgSize = mxGetDimensions(data0);

	// set image size values
	fun_image_size_values(imgSize,numDims,sy,sx,numChannels,numImgs);
	
	// fern data size
	const int *dataSize = mxGetDimensions(data1);
	int numFerns = dataSize[2];  // num. random ferns
	int numFeats = dataSize[0];  // num. binary features per fern
	
	// num. fern outputs : histogram bins
	int numBins = (int)pow(2,numFeats);
	
	// output: fern output maps
	int out[4];
	out[0] = sy;
	out[1] = sx;
	out[2] = numFerns;
	out[3] = numImgs;
	plhs[0] = mxCreateNumericArray(4, out, mxDOUBLE_CLASS, mxREAL);
	double *fernMaps = (double *)mxGetPr(plhs[0]);
	
	// variables
	int px,py,pc,z;
	double va,vb;
	int tmp1 = sy*sx;	
	int tmp2 = numFeats*6;
	int tmp3 = tmp1*numChannels;
	int tmp4 = tmp1*numFerns;

	// random ferns
	for (int fern=0; fern<numFerns; fern++){
	
		// convolution 
		for (int x=0; x<=sx-fernSize; x++) {
			for (int y=0; y<=sy-fernSize; y++) {

				// images
				for (int img=0; img<numImgs; img++){
	
					// fern output
					z = 1;

					// test fern features
					for (int feat=0; feat<numFeats; feat++){

						// point A coordinates		
						py = y + (int)*(data + feat + 0*numFeats + fern*tmp2); 
						px = x + (int)*(data + feat + 1*numFeats + fern*tmp2); 
						pc =     (int)*(data + feat + 2*numFeats + fern*tmp2); 
						va = *(imgs + py + px*sy + pc*tmp1 + img*tmp3);

						// point B coordinates		
						py = y + (int)*(data + feat + 3*numFeats + fern*tmp2); 
						px = x + (int)*(data + feat + 4*numFeats + fern*tmp2); 
						pc =     (int)*(data + feat + 5*numFeats + fern*tmp2); 
						vb = *(imgs + py + px*sy + pc*tmp1 + img*tmp3);

						// point comparison
						//if (va>vb){
						//	z+= (int)pow(2,feat);
						//}
						//z += (1 << feat) & (0 - (va > vb));
						z += (1 << feat) & (0 - ((va - vb) > 0.01));
			
					} 
					
					// fern output map
					*(fernMaps + y + x*sy + fern*tmp1 + img*tmp4) = z;

				}
			}
		}
	}
}
