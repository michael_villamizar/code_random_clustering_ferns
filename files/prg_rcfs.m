%Random Clustering Ferns (RCFs)
%
%Description:
%  This code computes Random Clustering Ferns (RCFs) to recognize objects 
%  exhibiting multiple intra-class modes, where each one is associated to a 
%  particular object appearance [1]. In particular, RCFs use Boosted Random 
%  Ferns (BRFs) [2] and probabilistic Latent Semantic Analysis (pLSA) [3] to 
%  obtain a discriminative and multimodal classifier that automatically clusters
%  the response of its randomized trees in function of the visual object 
%  appearance.
%
%  For further information about RCFs, please, refer to references [1][2][3].
%
%Comments:
%  The parameters of the proposed RCFs can be found in the fun_parameters.m 
%  function (/files/functions/). The fun_experiments.m file allows to compute 
%  and evaluate different multimodal classifiers, each one computed with 
%  different parameters.
%
%  To compute and evaluate the RCFs, a subset of the Caltech Faces Dataset [4] 
%  is used. Specifically, 100 images containing five people are used together
%  100 background images collected from Internet. If anyone uses this dataset, 
%  please cite the reference [4].
%
%  If you make use of this code, we kindly encourage to cite the reference [1], 
%  listed below. This code is only for research and educational purposes. 
%
%Steps:
%  Steps to execute the program:
%    1. Run the prg_setup.m file to configure the program paths.
%    2. Run the prg_rcfs.m file (/files/) to compute the RCFs and to perform 
%       face detection and clustering over the given dataset. 
%    3. Observe the output (images and detection data) of RCFs in the results 
%       folder (/results/exp_t1/detections/).
%    4. Run the fun_caltech_faces_evaluation.m (/files/datasets/) to compute 
%       the detection and clustering performance rates of RCFs. 
%
%References:
%  [1] Multimodal Object Classification using Random Clustering Trees. 
%      M. Villamizar, A. Garrell, A. Sanfeliu and F. Moreno-Noguer. Iberian 
%      Conference on Pattern Recognition and Image Analysis (IbPRIA), 2015.
%
%  [2] Bootstrapping Boosted Random Ferns for Discriminative and Efficient
%      Object Classification. M. Villamizar, J. Andrade-Cetto, A. Sanfeliu
%      and F. Moreno-Noguer. Pattern Recognition, 2012.
%
%  [3] Unsupervised learning by probabilistic latent semantic analysis. 
%      T. Hofmann. Machine learning, 42(1-2):177–196, 2001.
%
%  [4] Object class recognition by unsupervised scale-invariant learning.
%      R. Fergus, P. Perona, and A. Zisserman. CVPR, 2003.
%
%Contact: 
%  Michael Villamizar
%  mvillami@iri.upc.edu
%  http://www.iri.upc.edu/people/mvillami/
%  Institut de Robòtica i Informàtica Industrial CSIC-UPC
%  Barcelona - Spain
%  2016

%% main function
function prg_rcfs()
clc, close all,clear all

% message
fun_messages('Random Clustering Ferns (RCFs)','presentation');

% global variables
global iterExp;

% experiments
for iterExp = 1:1

	% delete variables
	fun_delete_variables();

	% train RCFs
	RCFs = fun_random_clustering_ferns();

	% test
	fun_test(RCFs);
    
	% copy and save results
	fun_save_results();
end

% message
fun_messages('End','title');
end
