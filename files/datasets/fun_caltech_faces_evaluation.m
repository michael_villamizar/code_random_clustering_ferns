%% Caltech faces evaluation
function fun_caltech_faces_evaluation()
clc;close all;clear all;

% message
fun_messages('Caltech Faces Evaluation','title');

% parameters
lw = 4;  % line width
fs = 18;  % font size
imgPath = ['./datasets/caltech_faces/images/'];  % images path
annPath = ['./datasets/caltech_faces/annotations/'];  % annotations path
detPath = ['./results/exp_t1/detections/data/'];  % detection results path
intThr = 0.1;  % intersection -overlapping- threshold
minThr = 0.0;  % min. classifier threshold
maxThr = 1.5;  % max. classifier threshold
numClusters = 5;  % num. intra-class clusters

% Classifier evaluation: This function measures the performance of the Boosted
% Random Ferns (BRFs) classifier to detect faces in the Caltech Face Dataset.
% The evaluation is carried out using the ground truth of the dataset and the
% detections provided by the classifier. The classifier performance is measured
% using the recall, precision, and f-measure rates for varying thresholds of 
% the classifier.
[rates,EER] = fun_classifier_evaluation(detPath,annPath,minThr,maxThr,intThr);

% show classification rates
figure,subplot(121),plot(rates.thr,rates.rec,'r-','linewidth',lw),hold on;
plot(rates.thr,rates.pre,'b-','linewidth',lw), hold on;
plot(rates.thr,rates.fme,'k-','linewidth',lw), grid on;
xlabel('Thresholds','fontsize',fs);
ylabel('Rates','fontsize',fs);
legend('Recall','Precision','F-Measure');

% messages
fun_messages('Classification rates','process');
fun_messages(sprintf('Recall: %.3f',EER.rec),'information');
fun_messages(sprintf('Precision: %.3f',EER.pre),'information');
fun_messages(sprintf('F-Measure: %.3f',EER.fme),'information');
fun_messages(sprintf('Equal Error Rate (EER): %.3f',EER.val),'information');
fun_messages(sprintf('Classifier threshold: %.3f',EER.thr),'information');

% Clustering evaluation: This function measures the clustering performance of
% probabilistic Latent Semantic Analysis (pLSA) to distinguish different people
% in the Caltech Face Dataset. The evaluation is conducted using a entropy-
% based metric on the confusion matrix since the intra-class clusters labels in
% the training data not necessary correspond to the labels provided by pLSA. 
[cnfMat,cnfEnt] = fun_clustering_evaluation(detPath,annPath,numClusters,...
	intThr,EER.thr);

% show clustering results
subplot(122),imagesc(cnfMat),colormap(gray);
xlabel('Estimated Labels','fontsize',fs);
ylabel('Ground Truth','fontsize',fs);
 
% messages
fun_messages('Clustering rate','process');
fun_messages(sprintf('Entropy: %.3f',cnfEnt),'information');

% message
fun_messages('End','title');
end

%% Classifier evaluation
% This function measures the performance of the Boosted Random Ferns (BRFs) 
% classifier to detect faces in the Caltech Face Dataset. The evaluation is 
% carried out using the ground truth of the dataset and the detections provided
% by the classifier. The classifier performance is measured using the recall, 
% precision, and f-measure rates for varying thresholds of the classifier.
function [output1,output2] = fun_classifier_evaluation(detPath,annPath,...
	minThr,maxThr,intThr)
if (nargin<5),fun_messages('Incorrect parameters','error'); end

% detection files
detFiles = dir([detPath,'*.mat']);
numFiles = size(detFiles,1);

% messages
fun_messages('BRFs classifier evaluation','process');
fun_messages(sprintf('num. detection files: %d',numFiles),'information');

% variables
cont = 0;  % counter

% threshold step
step = (maxThr-minThr)/49;

% thresholds
for iterThr = minThr:step:maxThr

	% update counter
	cont = cont + 1;

	% variables
	fps = 0;  % false positives
	fns = 0;  % false negatives
	tps = 0;  % true positives

	% detection files
	for iterFile = 1:numFiles

		% detection file
		name = detFiles(iterFile).name;  % name
		annData = fun_data_load(annPath,name);  % annotation data
		if (annData.numObjects~=1), error('Incorrect annotation'); end

		% annotation box
		bbox = annData.bbox;

		% detection data
		detData = fun_data_load(detPath,name);

		% thresholding
		indxs = find(detData.scores >= iterThr);
		boxes = detData.boxes(indxs,:);
		scores = detData.scores(indxs);
		clusters = detData.clusters(indxs);

		% non-maxima supression	
		[boxes,scores,clusters] = fun_non_maxima_suppression(boxes,...
			scores,clusters,intThr);

		% num. detections
		numDets = size(boxes,1);

		% intersection -overlapping- between boxes
		ovlp = fun_boxes_intersection(bbox,boxes,0.5);

		% classification (for one object annotation)
		tmp = min(1,sum(ovlp.indexes,2));  % temporal variable
		tps = tps + tmp;  % true positives
		fps = fps + numDets - tmp;  % false positives
		fns = fns + 1 - tmp;  % false negatives
	end

	% classification rates
	rec = tps/(tps + fns);  % recall
	pre = tps/(tps + fps);  % precision
	fme = 2*rec*pre/(rec+pre);  % f-measure

	% results
	rates.rec(cont) = rec;  % recall
	rates.pre(cont) = pre;  % precision
	rates.fme(cont) = fme;  % f-measure
	rates.thr(cont) = iterThr;  % classifier threshold
end

% Equal Error Rate (EER)
[~,ind] = min(abs(rates.rec - rates.pre));
EER.val = 0.5*(rates.rec(ind)+rates.pre(ind));  % EER: value
EER.thr = rates.thr(ind);  % EER: threshold
EER.rec = rates.rec(ind);  % EER: recall
EER.pre = rates.pre(ind);  % EER: precision
EER.fme = rates.fme(ind);  % EER: f-measure
if (EER.val==0), fun_messages('Incorrect EER value','error'); end

% output
output1 = rates;
output2 = EER;
end

%% Clustering evaluation
% This function measures the clustering performance of probabilistic Latent 
% Semantic Analysis (pLSA) to distinguish different people in the Caltech Face
% Dataset. The evaluation is conducted using a entropy-based metric on the 
% confusion matrix since the intra-class clusters labels in the training data
% not necessary correspond to the labels provided by pLSA. 
function [output1,output2] = fun_clustering_evaluation(detPath,annPath,...
	numClusters,intThr,detThr)
if (nargin<5),fun_messages('Incorrect parameters','error'); end

% detection files
detFiles = dir([detPath,'*.mat']);
numFiles = size(detFiles,1);

% messages
fun_messages('pLSA evaluation','process');
fun_messages(sprintf('num. detection files: %d',numFiles),'information');
fun_messages('Thresholding','information');

% variables
mat = zeros(numClusters,numClusters);  % conf. matrix

% detection files
for iterFile = 1:numFiles

	% detection file
	name = detFiles(iterFile).name;  % name
	annData = fun_data_load(annPath,name);  % annotation data
	if (annData.numObjects~=1), error('Incorrect annotation'); end

	% annotation box
	bbox = annData.bbox;

	% detection data
	detData = fun_data_load(detPath,name);

	% thresholding
	indxs = find(detData.scores >= detThr);
	boxes = detData.boxes(indxs,:);
	scores = detData.scores(indxs);
	clusters = detData.clusters(indxs);

	% non-maxima supression	
	[boxes,scores,clusters] = fun_non_maxima_suppression(boxes,...
		scores,clusters,intThr);

	% num. detections
	numDets = size(boxes,1);

	% Ground truth labels: The intra-class clusters labels corresponds to
	% people identity. Five people are considered in this experiment, each
        % having 20 images. Hence, the first 20 images have label one, whereas
	% the last 20 images have label five.
	label = ceil(iterFile/20);

	% Confusion matrix: This builds the confusion matrix using the ground 
	% truh labels and estimated labels. Since pLSA determines automatically
	% the visual appearance clusters, this matrix is not necessary diagonal.
	% For example, the first 20 images can be assign to label two.
	for iterDet = 1:numDets
		c = clusters(iterDet);  % estimated cluster label
		mat(label,c) = mat(label,c) + 1;  % update matrix values
	end

end

% Clustering error: Since the confusion matrix is not diagonal, an entropy-
% based metric is used to measure the clustering performance. A low entropy
% value means that the clustering performance is good.
ent = 0;  % entropy error
for iter = 1:numClusters
	v = mat(:,iter)'+ eps;
	v = v./sum(v(:));
	ent = ent + sum(v.*log2(v),2);
end
ent = -1*ent/numClusters;

% output
output1 = mat;
output2 = ent;
end

%% Boxes intersection
% This function measures the overlapping -intersection- between two sets of
% bounding boxes. The function also performs boxes matching using the input 
% intersecion threshold -intThr-.
function output = fun_boxes_intersection(boxesA,boxesB,intThr)
if (nargin<3),fun_messages('Incorrect parameters','error'); end

% num. boxes
numBoxesA = size(boxesA,1);
numBoxesB = size(boxesB,1);

% allocate
matVal = zeros(numBoxesA,numBoxesB);
matInd = zeros(numBoxesA,numBoxesB);

% boxes A 
for iterA = 1:numBoxesA
    % box A
    boxA = boxesA(iterA,:);
    % boxes B 
    for iterB = 1:numBoxesB
        
        % box B
        boxB = boxesB(iterB,:);
        
        % intersection area
        x1 = max(boxA(1),boxB(1));
        y1 = max(boxA(2),boxB(2));
        x2 = min(boxA(3),boxB(3));
        y2 = min(boxA(4),boxB(4));
        intArea = (x2-x1)*(y2-y1);
        
        % union area
        areaA = (boxA(3)-boxA(1))*(boxA(4)-boxA(2));
        areaB = (boxB(3)-boxB(1))*(boxB(4)-boxB(2));
        uniArea = areaA + areaB - intArea;
        
        % metric value
        val = intArea/uniArea;
        
        % check : no intersection
        if ((x2-x1<0) || (y2-y1<0)), val = 0; end
        
        % save value
        matVal(iterA,iterB) = val;
    end
end

% sort values
[vals,inds] = sort(matVal(:));

% classification
for iter = size(vals,1):-1:1
    % current index
    [i,j] = ind2sub(size(matVal),inds(iter));
    % matching
    if (sum(matInd(:,j),1)==0)&&(sum(matInd(i,:),2)==0)&&matVal(i,j)>=intThr
        matInd(i,j)= 1;
    end
end

% overlapping data
data.values = matVal;
data.indexes = matInd;

% output
output = data;
end
