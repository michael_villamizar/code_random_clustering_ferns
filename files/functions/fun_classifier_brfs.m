%% Classifier: Boosted Random Ferns (BRFs)
% This function computes the boosted random ferns classifier using the input 
% training samples and the pool of shared random ferns.
function output = fun_classifier_brfs(samples,ferns)
if (nargin<2),fun_messages('Incorrect parameters','error'); end

% parameters
prms = fun_parameters();  % program parameters
numWCs = prms.classifier.numWCs;  % num. weak classifiers (WCs)
objSize = prms.classifier.objSize;  % object size
numFeats = prms.ferns.numFeats;  % num. binary features per fern
numBins = 2^numFeats;  % num. histogram bins (fern outputs)
numPosSamples = samples.numPosSamples;  % num. positive image samples
numNegSamples = samples.numNegSamples;  % num. negative image samples

% Potential Weak Classifiers (PWCs): This computes the pool of all possible 
% weak classifiers used to compute the object classifier. Each potential weak 
% classifier corresponds to a random fern tested in a particular image location
% [y,x]. Boosting selects the most discriminative weak classifiers (WCs) from 
% this pool of weak classifiers (PWCs).
PWCs = fun_potential_weak_classifiers();
numPWCs = size(PWCs,1);

% Fern maps: The set of random ferns are convolved over the input samples in 
% order to compute the outputs of ferns associated with the Potential Weak 
% Classifiers (PWCs). This also alliviates the computational cost.
posFernMaps = mex_fern_maps(samples.positives,ferns.data,ferns.fernSize);
negFernMaps = mex_fern_maps(samples.negatives,ferns.data,ferns.fernSize);

% messages
fun_messages('Classifier: Boosted Random Ferns (BRFs)','process');
fun_messages(sprintf('Num. weak classifiers: %d',numWCs),'information');
fun_messages(sprintf('Num. potential weak classifiers: %d',numPWCs),...
	'information');

% test potential weak classifiers over images -fern maps-
posOutputs = fun_test_potential_weak_classifiers(PWCs,posFernMaps);
negOutputs = fun_test_potential_weak_classifiers(PWCs,negFernMaps);

% num. input samples
numSamples = numPosSamples + numNegSamples;

% initial sample weights
weights = (1/numSamples)*ones(1,numSamples);

% sample labels: +1/-1
labels = [ones(1,numPosSamples), -1*ones(1,numNegSamples)];

% allocate
WCs = zeros(numWCs,3);  % weak classifiers (WCs) data
hstms = zeros(numWCs,numBins);  % weak classifiers probabilities -histograms-
indexes = zeros(1,numWCs);  % selected weak classifiers indexes    
posDetMaps = zeros(numWCs,numPosSamples);  % positive detection map
negDetMaps = zeros(numWCs,numNegSamples);  % negative detection map

% boosting 
for iter = 1:numWCs
    
	% weights -positive and negative-
	posWeights = weights(1,1:numPosSamples);
	negWeights = weights(1,1+numPosSamples:end);

	% Weak learner: This function chooses the most discriminative Weak 
	% Classifier (WC) from the pool of Potential Weak Classifiers (PWCs). 
	% This is done in accordance to the current samples weights and the 
	% classification error.
	WC = fun_weak_learner(PWCs,posOutputs,negOutputs,posWeights,...
		negWeights,numBins,indexes);

	% test the selected weak classifier
	posDetVals = fun_test_weak_classifier(WC,posOutputs);
	negDetVals = fun_test_weak_classifier(WC,negOutputs);

	% update sample weights
	weights = weights.*(exp(-1*labels.*[posDetVals,negDetVals]));

	% weight normalization
	weights = weights./sum(weights(:));      

	% update index list
	indexes(1,iter) = WC.index;

	% save -chosen -weak classifier data
	WCs(iter,:) = WC.data;  % weak classifier data
	hstms(iter,:) = WC.hstm;  % weak classifier probabilities
	posDetMaps(iter,:) = posDetVals;  % positive detection maps
	negDetMaps(iter,:) = negDetVals;  % negative detection maps

	% message
	if(mod(iter,25)==0), fun_messages(sprintf('Weak classifier: %d',...
		iter),'information'); end
end

% detection map
fun_detection_map(posDetMaps,negDetMaps);

% classifier: BRFs
BRFs.prms = prms;  % program parameters
BRFs.WCs = WCs;  % weak classifiers data
BRFs.hstms = hstms;  % weak classifier probabilities
BRFs.ferns = ferns;  % shared random ferns
BRFs.numWCs = numWCs;  % num. weak classifiers
BRFs.objSize = objSize;  % object size

% output
output = BRFs;
end

%% Potential Weak Classifiers (PWCs)
% This function computes the pool of weak classifier candidates, which are then
% chosen by the learning algorithm to compute the object classifier. Each 
% potential weak classifier corresponds to a fern computed at particular image
% location (y,x).
function output = fun_potential_weak_classifiers()

% parameters
prms = fun_parameters();  % program parameters                                            
objSize = prms.classifier.objSize;  % object size                             
numFerns = prms.ferns.numFerns;  % num. shared random ferns                           
fernSize = prms.ferns.fernSize;  % fern size -spatial size-                          
numMaxPWCs = prms.classifier.numMaxPWCs;  % num. max. PWCs

% variables
cont = 0;  % counter                        

% image limits
ly = objSize(1) - fernSize + 1;
lx = objSize(2) - fernSize + 1;

% vectors
vy = 1:ly;
vx = 1:lx;

% num. potential weak classifiers
numPWCs = size(vy,2)*size(vx,2)*numFerns;

% allocate
PWCs = zeros(numPWCs,3);

% create potential weak classifiers
for iterFern = 1:numFerns
	for iterY = 1:ly
		for iterX = 1:lx
			% update counter
			cont = cont + 1;
			% weak classifier data: location and fern index
			PWCs(cont,:) = [iterY,iterX,iterFern];
		end
	end
end

% check
if(cont~=numPWCs),fun_messages('Incorrect potential weak classifiers',...
	'error'); end

% reduce num. PWCs
if (cont>numMaxPWCs)
	% message
	fun_messages(sprintf('Reducing the num. of PWCS from %d to %d',...
		numPWCs,numMaxPWCs),'warning');
	% random permutation -> random indexes
	indxs = randperm(cont);
	PWCs = PWCs(indxs(1:numMaxPWCs),:);
end

% output
output = PWCs;
end

%% Weak learner
% This function chooses the most discriminative Weak Classifier (WC) from the 
% pool of Potential Weak Classifiers (PWCs). This is done in accordance to 
% the current samples weights and the classification error.
function output = fun_weak_learner(PWCs,posOutputs,negOutputs,posWeights,...
	negWeights,numBins,indexes)
if (nargin<7),fun_messages('Incorrect parameters','error'); end

% parameters
prms = fun_parameters();  % program parameters
eps = prms.classifier.eps;  % epsilon value

% fern output histograms
posHstms = fun_hstms(posWeights,posOutputs,numBins);
negHstms = fun_hstms(negWeights,negOutputs,numBins);

% num. potential weak classifiers
numPWCs = size(posOutputs,1);

% max. distance between distributions
Qmax = inf;

% test potential weak classifiers
for iterPWC = 1:numPWCs

	% positive and negative histograms
	posHstm(1,:) = posHstms(iterPWC,:);     
	negHstm(1,:) = negHstms(iterPWC,:);

	% normalization
	posHstm = fun_normalize(posHstm);
	negHstm = fun_normalize(negHstm);

	% check
	if (sum(posHstm(:))<=0), continue; end
	if (sum(negHstm(:))<=0), continue; end
    
	% distance between distributions
	Q = 2*sum(sqrt(posHstm.*negHstm),2);

	% min. distance
	if (Q<Qmax)

		% check repeated weak classifiers
		if (sum(indexes==iterPWC,2)==0)

			% update
			Qmax = Q;

			% log. ratio of fern probabilities
			logHstm = 0.5*log((posHstm+eps)./(negHstm+eps));

			% Weak Classifier (WC) 
			WC.hstm = logHstm;  % WC histogram
			WC.data = PWCs(iterPWC,:);  % WC data
			WC.index = iterPWC;  % WC index
		end
	end
end

% output
output = WC;
end

%% Ferns histograms
function output = fun_hstms(weights,fernOutps,numBins)
if (nargin<3),fun_messages('Incorrect parameters','error'); end

% num. potential weak classifiers and samples
[numPWCs,numSamples] = size(fernOutps);

% allocate
hstms = zeros(numPWCs,numBins);

% samples
for iterWC = 1:numPWCs
	for iterSample = 1:numSamples
		% fern output
		z = fernOutps(iterWC,iterSample);
		% update histogram
		hstms(iterWC,z) = hstms(iterWC,z) + weights(iterSample);
	end
end

% output
output = hstms;
end

%% Test weak classifier
function output = fun_test_weak_classifier(WC,fernOutps)
if (nargin<2),fun_messages('Incorrect parameters','error'); end

% num. samples
numSamples = size(fernOutps,2);

% allocate
detVals = zeros(1,numSamples);

% samples
for iterSample = 1:numSamples
	% fern output
	z = fernOutps(WC.index,iterSample);
	% detection values
	detVals(1,iterSample) = WC.hstm(1,z);
end

% output
output = detVals;
end

%% Normalization
% This function normalizes the input histogram to 1.
function output = fun_normalize(hstm)
if (nargin~=1), fun_messages('Incorrect input parameters','error'); end

% normalize histogram
if (sum(hstm(:))>0), hstm = hstm./sum(hstm(:)); end

% output
output = hstm;
end

%% Detection map
% This function shows the detection performance of the computed classifier
% over the training samples. The function plots the classifier confidence 
% of weak classifiers over the samples.
function fun_detection_map(posMap,negMap)
if (nargin~=2), fun_messages('Incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % program parameters
fs = prms.visualization.fontSize;  % font size
lw = prms.visualization.lineWidth;  % line width

% classifier confidence
posCurve = sum(posMap,1);
negCurve = sum(negMap,1);

% rough normalization
numWCs = size(posMap,1);
posCurve = posCurve./numWCs;
negCurve = negCurve./numWCs;

% show maps
figure,subplot(211),imagesc([posMap negMap]),colormap(gray);
xlabel('Samples','fontsize',fs);
ylabel('WCs','fontsize',fs);
title('Weak Classifiers Outputs','fontsize',fs);
subplot(212),plot(posCurve,'g','linewidth',lw),hold on,
plot(negCurve,'r','linewidth',lw),grid on;
xlabel('Samples','fontsize',fs);
ylabel('Confidence','fontsize',fs);
title('Classifier Confidence','fontsize',fs);
legend('Positive','Negative');

end
